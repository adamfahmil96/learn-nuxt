# learn-nuxt

Learn Nuxt JS

## Initial

Inisialisasi package.json
```
npm init -y 
```

Instal Nuxt.JS
```
npm install nuxt
```

Menjalankan project
```
npm run dev
```

Membuat app Nuxt dari npx
```
npx create-nuxt-app <nama-project>
```

## Sources for Learning

- [Baledemy: Playlist 'Tutorial Nuxtjs Indonesia'](https://youtube.com/playlist?list=PL9At9z2rvOC8xDXO3gjWpjb1Twia0DONV)
